<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

use App\Models\PropertyModel as Property;
use App\Models\PropertyAnalyticModel as PropertyAnalytic;
use App\Http\Resources\PropertyAnalytic as PropertyAnalyticResource;
use App\Http\Resources\PropertySummery as PropertySummeryResource;

class PropertySummeryController extends BaseController
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($col,$val)
    {
        $summery = Property::Select(
        'properties.id',
        'properties.suburb',
        'properties.state',
        'properties.country',
        \DB::raw("MIN(property_analytics.value) AS min_value"), 
        \DB::raw("MAX(property_analytics.value) AS max_value"),
        \DB::raw("AVG(property_analytics.value) AS median_value")
        )
        ->Join('property_analytics', 'property_analytics.property_id' , '=','properties.id' )
        ->where("properties.$col",$val)
        ->groupBy('properties.id')
        ->get();
        return $this->sendResponse(PropertySummeryResource::collection($summery), 'Properties summery retrieved successfully.');
    }

}
