<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

use App\Models\PropertyAnalyticModel as PropertyAnalytic;
use App\Http\Resources\PropertyAnalytic as PropertyAnalyticResource;

class PropertyAnalyticController extends BaseController
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($pid)
    {
        $analytics = PropertyAnalytic::where('property_id',$pid)->get();
        return $this->sendResponse(PropertyAnalyticResource::collection($analytics), 'Properties analytics retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();   
        $validator = Validator::make($input, [
            'property_id' => 'required',
            'analytic_type_id' => 'required',
            'value' => 'required'
        ]);
            
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $property = PropertyAnalytic::create($input);
        return $this->sendResponse(new PropertyAnalyticResource($property), 'Property analytic created successfully.');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($pid,$id)
    {

        $propertyAnalytic = PropertyAnalytic::find($id);
        if (is_null($propertyAnalytic)) {
            return $this->sendError('Property Ananlytic not found.');
        }
        return $this->sendResponse(new PropertyAnalyticResource($propertyAnalytic), 'Property analytic retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($pid,$id,Request $request, PropertyAnalytic $propertyAnalytic)
    {
        // dd($pid,$id);
        $input = $request->all();
        
        $propertyAnalytic = PropertyAnalytic::find($id);
        if (is_null($propertyAnalytic)) {
            return $this->sendError('Property Ananlytic not found.');
        }

        // dd($input,$propertyAnalytic);
        $validator = Validator::make($input, [
            'property_id' => 'required',
            'analytic_type_id' => 'required',
            'value' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $propertyAnalytic->property_id = $input['property_id'];
        $propertyAnalytic->analytic_type_id = $input['analytic_type_id'];
        $propertyAnalytic->value = $input['value'];
        $propertyAnalytic->save();
   
        return $this->sendResponse(new PropertyAnalyticResource($propertyAnalytic), 'Property analytic updated successfully.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($pid,$id,PropertyAnalytic $propertyAnalytic)
    {

        $propertyAnalytic = PropertyAnalytic::find($id);
        if (is_null($propertyAnalytic)) {
            return $this->sendError('Property analytic not found.');
        }

        $propertyAnalytic->delete();
        return $this->sendResponse([], 'Property analytic deleted successfully.');
    }
}
