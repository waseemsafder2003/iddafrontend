<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AnalyticTypeModel as AnalyticType;
use App\Models\PropertyModel as Property;

class PropertyAnalyticModel extends Model
{

    protected $table = 'property_analytics'; 

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id', 
        'analytic_type_id', 
        'value',
    ];

    public function AnalyticType()
    {
        return $this->belongsTo(AnalyticType::class,'analytic_type_id','id');
    }

    public function getAnalyticTypeNameAttribute($value)
    {
        $type = $this->AnalyticType()->first();
        return ucfirst($type->name);
    }

    public function Properties()
    {
        return $this->join('properties', 'properties.id', '=', 'property_analytics.property_id');
    }

}
