<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PropertyAnalyticModel as PropertyAnalytic;

class PropertyModel extends Model
{

    protected $table = 'properties'; 

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guid', 
        'state',
        'suburb',
        'country',
    ];

    public function Anylytics()
    {
        return $this->belongsToMany(PropertyAnalytic::class,'id','property_id');
    }
    
}
